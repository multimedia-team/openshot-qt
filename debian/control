Source: openshot-qt
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: video
Testsuite: autopkgtest-pkg-python
Priority: optional
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-sphinxdoc,
               python3,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               python3-pyqt5
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/multimedia-team/openshot-qt
Vcs-Git: https://salsa.debian.org/multimedia-team/openshot-qt.git
Homepage: https://www.openshot.org/

Package: openshot-qt
Architecture: all
Depends: fonts-cantarell,
         libjs-jquery,
         libjs-jquery-ui,
         python3-openshot (>= 0.3.2),
         python3-pkg-resources,
         python3-pyqt5,
         python3-pyqt5.qtsvg,
         python3-pyqt5.qtwebengine,
         python3-requests,
         python3-zmq,
         ${misc:Depends},
         ${python3:Depends}
Suggests: blender,
          inkscape,
          openshot-qt-doc,
Description: create and edit videos and movies
 OpenShot Video Editor is a free, open-source, non-linear video editor. It
 can create and edit videos and movies using many popular video, audio, and
 image formats. Create videos for YouTube, Flickr, Vimeo, Metacafe, iPod,
 Xbox, and many more common formats!
 .
 Features include:
  * Multiple tracks (layers)
  * Compositing, image overlays, and watermarks
  * Support for image sequences (rotoscoping)
  * Key-frame animation
  * Video and audio effects (chroma-key)
  * Transitions (lumas and masks)
  * 3D animation (titles and physics simulations)
  * Chroma key (green screen & blue screen)
  * Transcode (convert video encodings)
  * Upload videos (YouTube and Vimeo supported)

Package: openshot-qt-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-jquery,
         libjs-underscore,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Description: documentation for openshot-qt
 This is the official help manual for OpenShot Video Editor, a free,
 open-source, non-linear video editor. Learn how to edit videos, add
 watermarks, apply effects such as chroma key, and transition between
 videos. Also, learn how to become a developer, with step by step
 instructions.
